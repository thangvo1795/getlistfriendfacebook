package com.piiship.thangdev.presenter.result;

import com.facebook.AccessToken;

/**
 * Created by Thang Vo on 12/1/2016.
 */

public interface PresenterImpResult {
    void GetFriendList(AccessToken accessToken);
    void LoadMore(String graphPath);
}
