package com.piiship.thangdev.views.login;

/**
 * Created by Thang Vo on 11/29/2016.
 */

public interface ViewLogin {
    void LoginSuccess();
    void LoginCancel();
    void LoginError(String exception);
}
