package com.piiship.thangdev.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Thang Vo on 12/6/2016.
 */

public class Service {
    public static String DocNoiDungUrl(String theUrl){
        StringBuilder content = new StringBuilder();

        try{
            // create a url object
            URL url = new URL(theUrl);

            // create a urlconnection object
            URLConnection urlConnection = url.openConnection();

            // wrap the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;

            // read from the urlconnection via the bufferedreader
            while ((line = bufferedReader.readLine()) != null){
                content.append(line + "\n");
            }
            bufferedReader.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return content.toString();
    }
}
