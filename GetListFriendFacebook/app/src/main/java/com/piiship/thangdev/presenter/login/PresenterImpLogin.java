package com.piiship.thangdev.presenter.login;

import com.facebook.CallbackManager;

/**
 * Created by Thang Vo on 11/29/2016.
 */

public interface PresenterImpLogin {
    void CheckLogin(CallbackManager callbackManager);
}
