package com.piiship.thangdev.views.login;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.widget.LoginButton;
import com.piiship.thangdev.getlistfriendfacebook.R;
import com.piiship.thangdev.presenter.login.PresenterLogicLogin;
import com.piiship.thangdev.views.result.ResultActivity;

import java.security.MessageDigest;

public class LoginActivity extends AppCompatActivity implements ViewLogin{
    PresenterLogicLogin loginLogin;
    CallbackManager callbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        // set quyền friend
        loginButton.setReadPermissions("user_friends");

        //nếu có AccessToken thì chuyển qua activity Result
        if(AccessToken.getCurrentAccessToken()!=null){
            startActivity(new Intent(this,ResultActivity.class));
        }
        loginLogin = new PresenterLogicLogin(this);
        //kiểm tra đăng nhập
        loginLogin.CheckLogin(callbackManager);
     //   getKey();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    private void getKey() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo("com.piiship.thangdev.getlistfriendfacebook", PackageManager.GET_SIGNATURES);
            for(Signature signature: packageInfo.signatures){
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                String key = Base64.encodeToString(messageDigest.digest(),Base64.DEFAULT);
                Log.d("KeyHash", Base64.encodeToString(messageDigest.digest(),Base64.DEFAULT));
                Toast.makeText(this, key, Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
        e.printStackTrace();
        }
    }


    @Override
    public void LoginSuccess() {
        startActivity(new Intent(LoginActivity.this, ResultActivity.class));
    }

    @Override
    public void LoginCancel() {
        Toast.makeText(this, "user canceled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void LoginError(String e) {
        Toast.makeText(this, e, Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }


}
