package com.piiship.thangdev.presenter.login;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.piiship.thangdev.views.login.ViewLogin;

/**
 * Created by Thang Vo on 11/29/2016.
 */

public class PresenterLogicLogin implements PresenterImpLogin {
    ViewLogin viewLogin;
    public PresenterLogicLogin(ViewLogin viewLogin){
        this.viewLogin = viewLogin;
    }
    @Override
    public void CheckLogin(CallbackManager callbackManager) {

        LoginManager.getInstance()
                .registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        viewLogin.LoginSuccess();
                    }

                    @Override
                    public void onCancel() {
                        viewLogin.LoginCancel();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        viewLogin.LoginError(error.getMessage());
                    }

                });

    }
}
