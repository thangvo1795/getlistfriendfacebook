package com.piiship.thangdev.presenter.result;

import android.os.AsyncTask;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.piiship.thangdev.controller.Parser;
import com.piiship.thangdev.controller.Service;
import com.piiship.thangdev.views.result.ViewResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

/**
 * Created by Thang Vo on 12/1/2016.
 */

public class PresenterLogicResult implements PresenterImpResult {

    private ViewResult viewResult;
    private Parser parser;
    public PresenterLogicResult(ViewResult viewResult){
        parser = new Parser();
        this.viewResult = viewResult;
    }
    @Override
    public void GetFriendList(AccessToken accessToken) {
        new GraphRequest(
               accessToken,
                //get friend với limit là 10 friend
                "/me/taggable_friends?limit=10",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            // get list friend
                            JSONArray rawName = response.getJSONObject().getJSONArray("data");
                          //  Log.v("rawName",rawName+"");
                            String next = response.getJSONObject().getJSONObject("paging").getString("next");
                            viewResult.GetFriendList(parser.getFriendList(rawName),next);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }

    @Override
    public void  LoadMore(String graphPath) {

        try {
            //đọc nội dung từ graphPath để lấy danh sách friend
          String  result = new DocNoiDungWebsite().execute(graphPath).get();
            Log.v("result",result);
            JSONArray jsonArray = new JSONObject(result).getJSONArray("data");
            JSONObject paging = new JSONObject(result).getJSONObject("paging");
            String next=null;
            // nếu tồn tại object next thì lấy, k có thì trả về null
            // object next chứa danh sách friend tiếp theo
            if(paging.has("next"))
               next = paging.getString("next");
//            Log.v("next",next);
            viewResult.LoadMore(parser.getFriendList(jsonArray),next);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class DocNoiDungWebsite extends AsyncTask<String,Void,String>{
        @Override
        protected String doInBackground(String... strings) {
            return new Service().DocNoiDungUrl(strings[0]);
        }
    }

}
