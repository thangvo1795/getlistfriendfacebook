package com.piiship.thangdev.views.result;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.facebook.AccessToken;
import com.piiship.thangdev.getlistfriendfacebook.R;
import com.piiship.thangdev.models.User;
import com.piiship.thangdev.presenter.result.PresenterLogicResult;
import com.piiship.thangdev.views.adapter.ListFriendAdapter;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity implements ViewResult {
    ListView listView;
    private ListFriendAdapter adapter;
    private ArrayList<User> listFriend;
    private PresenterLogicResult logicResult;
    private ProgressDialog progressDialog;
    private int j=0;
    //gragPath là 1 URL chứa danh sách friend
    private String graphPath="";
    private View mFooterListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        listView = (ListView) findViewById(R.id.listView);
        logicResult = new PresenterLogicResult(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Vui lòng chờ");
        progressDialog.show();
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        // get list friend
        logicResult.GetFriendList(accessToken);
        //bắt sự kiện khi user scroll xuống item cuối cùng
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                if (absListView.getLastVisiblePosition() >= i2 - 1){
                    j++;
                    /*  - cái này vì khi vào app là nó vào cái method này 2 lần nên em cho 1 biến tạm là j để tránh
                     *  trường hợp đó và khi mà graphPath null thì nó sẽ k load nữa.
                     *  - Runable em cho nó dừng để thấy cái load more thôi chứ k có gì cả
                      */
                   if(j>2&&graphPath!=null){
                       new Handler().postDelayed(new Runnable() {
                           @Override
                           public void run() {

                               logicResult.LoadMore(graphPath);
                           }
                       },6000);

                    // khi mà graphPath = null nghĩa là k còn friend nữa nên remove nó ở listView
                   }else if(graphPath==null){
                       listView.removeFooterView(mFooterListView);
                   }
                }
            }
        });


    }

    @Override
    public void GetFriendList(ArrayList<User> arrayList,String graphPath) {
        progressDialog.dismiss();
        listFriend = arrayList;
        adapter = new ListFriendAdapter(this,listFriend);
        listView.setAdapter(adapter);
        // init listView và nhận danh sách friend tiếp theo
        mFooterListView = LayoutInflater.from(this).inflate(R.layout.view_load_more,null);
        listView.addFooterView(mFooterListView);
        this.graphPath = graphPath;
    }

    @Override
    public void LoadMore(ArrayList<User> arrayList,String graphPath) {
      this.graphPath = graphPath;
        // nhận danh sách friend mới và notify listView lại
        listFriend.addAll(arrayList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void Error(String error) {

    }
}
