package com.piiship.thangdev.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.piiship.thangdev.getlistfriendfacebook.R;
import com.piiship.thangdev.models.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Thang Vo on 12/1/2016.
 */

public class ListFriendAdapter extends BaseAdapter {

    private ArrayList<User> listUser;
    private Context ctx;
    public ListFriendAdapter(Context ctx, ArrayList<User> arrayList){
        this.ctx = ctx;
        listUser = arrayList;
    }
    @Override
    public int getCount() {
        return listUser.size();
    }

    @Override
    public User getItem(int i) {
        return listUser.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        User user = getItem(i);
        if(user!=null) {
            view = LayoutInflater.from(ctx).inflate(R.layout.view_list, viewGroup, false);
            ImageView imgAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
            TextView txtName = (TextView) view.findViewById(R.id.txtName);
            Picasso.with(ctx).load(user.getAvatar()).into(imgAvatar);
            txtName.setText(user.getName());
        }
        return view;
    }
}
