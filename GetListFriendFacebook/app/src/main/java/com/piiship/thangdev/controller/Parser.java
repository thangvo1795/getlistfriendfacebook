package com.piiship.thangdev.controller;

import com.piiship.thangdev.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Thang Vo on 12/1/2016.
 */

public class Parser {

    public ArrayList<User> getFriendList(JSONArray jsonArray) throws JSONException {
        ArrayList<User> arrayList = new ArrayList<>();
        for (int i=0;i<jsonArray.length();i++){
            JSONObject object = jsonArray.getJSONObject(i);
            String avatar= object.getJSONObject("picture").getJSONObject("data").getString("url");
            User user = new User(avatar,object.getString("name"));
            arrayList.add(user);
        }
        return arrayList;
    }



}
