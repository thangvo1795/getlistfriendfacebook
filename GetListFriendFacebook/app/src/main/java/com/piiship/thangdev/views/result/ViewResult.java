package com.piiship.thangdev.views.result;

import com.piiship.thangdev.models.User;

import java.util.ArrayList;

/**
 * Created by Thang Vo on 11/29/2016.
 */

public interface ViewResult {
    void GetFriendList(ArrayList<User>arrayList,String next);
    void LoadMore(ArrayList<User> arrayList,String graphPath);
    void Error(String error);

}
