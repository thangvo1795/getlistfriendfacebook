package com.piiship.thangdev.models;

/**
 * Created by Thang Vo on 11/29/2016.
 */

public class User {
  private String avatar;
    private String name;

    public User(String avatar, String name) {
        this.avatar = avatar;
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
